# flask-hostname

Simple container.

- Present a web response with the hostname of the current machine, with flask.
- Log that hostname to stdout.
